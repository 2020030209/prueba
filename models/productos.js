const json = require('express/lib/response');
const resolve = require('path/posix');
const promise =  require('../models/conexion.js');
const conexion = require('./conexion.js');

var ProductosDb= {};

ProductosDb.insertar = function insertar(productos){

    return new Promise((resolve, reject)=>{
        var sqlConsulta = 'insert into productos set ? ';
        conexion.query(sqlConsulta,productos, (err, res) => {

                if(err){
                    reject(err.message)
                }else{
                    resolve({
                        id_prod:productos.insertid_prod, 
                        codigo: productos.codigo,
                        nombre: productos.nombre,
                        descripcion : productos.descripcion,
                        color: productos.color,
                        material : productos.material,
                        existencia: productos.existencia,
                        precio: productos.precio,
                        filename:productos.filename,
                        path:productos.pathh
                })
                }

        })
    })
}

ProductosDb.mostrar = function mostrar(){
    let productos ={}
    return new Promise ((resolve, reject)=>{
        

        var sqlConsulta = 'select * from productos';
        conexion.query(sqlConsulta, null, function(err, res){
            
            if(err){
                reject(err.message);
            }else{
                
                productos=res;
                resolve(productos);
            }

        })
    })
}
ProductosDb.mostrarUno = function mostrarUno(codigo){
    let productos ={}
    return new Promise ((resolve, reject)=>{
        

        var sqlConsulta = 'select * from productos where codigo = ? ';
        conexion.query(sqlConsulta, codigo, function(err, res){
            
            if(err){
                reject(err.message);
            }else{
                
                productos=res;
                resolve(productos);
            }

        })
    })
}
ProductosDb.buscarProductos = function buscarProductos(nombre){
    let productos = {};
    return new Promise((resolve, reject)=>{
        

        var sqlConsulta = 'select * from productos where nombre = ?;';
        conexion.query(sqlConsulta, nombre, function(err, res){
            
            if(err){
                reject(err.message);
            }else{
                productos = res;
                resolve(res);
            }

        })
    })
}

//borrar por codigo

ProductosDb.borrar = function borrar(codigo){
    return new Promise((resolve, reject)=>{
        
        var sqlConsulta = 'delete from productos where codigo = ?;';
        conexion.query(sqlConsulta, codigo, function(err, res){
            
            if(err){
                reject(err.message);
            }else{
                resolve(res.affectedRows);
            }

        })
    })
}

//actualizar producto

ProductosDb.actualizar = function actualizar(nombre, descripcion, color, material, existencia, precio, filename, codigo){
    return new Promise((resolve, reject)=>{
        var sqlConsulta = 'update productos set nombre = ?, descripcion = ?, color = ?, material = ?, existencia = ?, precio = ?, filename = ? where codigo = ? ';
        conexion.query(sqlConsulta,[nombre, descripcion, color, material, existencia, precio, filename, codigo], function(err, res){

                if(err){
                    reject(err.message);
                }else{
                    resolve(res);
                }

        })
    })
}

module.exports = ProductosDb;