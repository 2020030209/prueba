

  function buscar(){
    const Nombre = document.getElementById('nombreBusqueda').value;  

     if(Nombre){
        axios.get('/buscar', {params: {nombre: Nombre}})
        .then((res)=>{
            mostrar(res.data);
        })
        .catch((error)=>{
            console.log(error);
            alert('No se encontró el producto');
        })
        function mostrar(data){
            const res = document.getElementById('tabla');
            res.innerHTML = "<tr><th>ID</th><th>Código</th><th>Nombre</th><th>Descripción</th><th>Color</th><th>Material</th><th>Exisstencia</th><th>Precio</th></tr>"
        
                for(item of data){
                    res.innerHTML += '<tr> <td>' + item.id_prod + '</td> <td>' + item.codigo + '</td> <td>'  + item.nombre + '</td> <td>' + item.descripcion + '</td><td>' + item.color + 
                    '</td> <td>' + item.material + '</td> <td>' + item.existencia + '</td> <td>' + item.precio + '</td> </tr>';
            }
        }
    }
    }

function eliminar(){
    const codigo = document.getElementById('codigo').value;

                if(codigo){
                    axios.delete('/borrar', {params: {codigo:codigo}})
                    .then((response) => {
                        console.log(response.data);
                        alert('Se eliminó correctamente');
                        const res = document.getElementById('tabla');
                        res.innerHTML = "<tr><th>ID</th><th>Nombre</th><th>Descripción</th><th>Color</th><th>Material</th><th>Exisstencia</th><th>Precio</th></tr>"
                        mostrar();
                    })
            .catch((error) => {
                console.log(error);
                alert('Algo salió mal');
            });
        }
        
        }

function actualizar(){
    const Codigo = document.getElementById('codigo').value;
    const Nombre = document.getElementById('nombre').value;
    const Descripcion = document.getElementById('descripcion').value;
    const Color = document.getElementById('color').value;
    const Existencia = document.getElementById('existencia').value;
    const Precio = document.getElementById('precio').value;
    const Material = document.getElementById('material').value;

        if(Codigo){
            const producto = {
                codigo : Codigo,
                nombre : Nombre,
                descripcion : Descripcion, 
                color : Color,
                material : Material,
                existencia : Existencia,
                precio : Precio
            }
    axios.put('/actualizar', producto)
    .then((response) => {
        console.log(response.data);
        alert('Se actualizó correctamente');
        const res = document.getElementById('tabla');
        res.innerHTML = "<tr><th>ID</th><th>Nombre</th><th>Descripción</th><th>Color</th><th>Material</th><th>Exisstencia</th><th>Precio</th></tr>"
        mostrar();
    })
    .catch((error) => {
        console.log(error);
        alert('Algo salió mal');
    });
}
}        