const http = require ("http");
const express = require("express");
const bodyparser= require("body-parser");
const morgan = require('morgan');
const misRutas = require ("./router/index");
const path = require ("path");
const multer = require('multer');
const uuid = require('uuid');
const { format } = require('timeago.js');

const app = express();
require('./models/conexion');

// settings
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine("html",require("ejs").renderFile);

// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
const storage = multer.diskStorage({
    destination: path.join(__dirname, 'public/img/uploads'),
    filename: (req, file, cb, filename) => {
        console.log(file);
        cb(null, "ImagenProducto" + path.extname(file.originalname));
    }
}) 
app.use(multer({storage}).single('image'));

// Global variables
app.use((req, res, next) => {
    app.locals.format = format;
    next();
});

// static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./router/index'));

const puerto=3005;
//const ip='34.203.33.75';
    app.listen(puerto, ()=>{

        console.log("Iniciado puerto "+puerto);
    });
