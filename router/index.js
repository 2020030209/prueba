const { Router } = require('express');

const path = require('path');
const { unlink } = require('fs-extra');
const router = Router();

// Models
const db = require("../models/productos")

router.get('/', async (req, res) => {
    const productos = await db.mostrar();
    res.render('Productos/Admin.html', { productos });
    res.render('Productos.html', { productos });
});

router.get('/administrador', async (req, res) => {
    const productos = await db.mostrar();
    res.render('Productos/Admin.html', { productos });
});

router.get('/insertar', (req, res) => {
    res.render('Productos/Insertar.html');
});

router.post('/insertar', async(req,res)=>{
    if ( req.body.codigo && req.body.nombre && req.body.descripcion && req.body.color && req.body.material && req.body.existencia && req.body.precio && req.file.filename) {
        const     producto = {
            codigo:req.body.codigo,
            nombre:req.body.nombre,
            descripcion:req.body.descripcion,
            color:req.body.color,
            material:req.body.material,
            existencia:req.body.existencia,
            precio:req.body.precio,
            filename:req.file.filename,
            pathh : '/img/uploads/' + req.file.filename
        };
        resultado = await db.insertar(producto);
        res.redirect('/administrador');
      } else {
        res.status(400).json({ error: 'Falta ingresar los campos obligatorios' });
      }
})
router.get('/producto/:codigo', async (req, res) => {
    const { codigo } = req.params;
    const productos = await db.mostrarUno(codigo);
    res.render('Productos/perfilProducto.html', { productos });
});

router.get('/producto/:codigo/delete', async (req, res) => {
    const { codigo } = req.params;
    await db.borrar(codigo);
    res.redirect('/administrador');
});

router.get('/producto/:codigo/actualizar', async (req, res) => {
    const { codigo } = req.params;
    const productos = await db.mostrarUno(codigo);
    res.render('Productos/Actualizar.html', {productos});
});


router.get('/actualizar', async(req,res)=>{
    const productos = req.body;
 
     db.actualizar(productos.nombre, productos.descripcion, productos.color, productos.material, productos.existencia, productos.precio, productos.filename, productos.codigo)
   
     resultado = await db.actualizar(productos);
     res.render('Productos/Actualizar.html', {productos});
     res.redirect('/administrador');
   
})
/*
router.get('/buscar', (req,res)=>{
    const nombre = req.query.nombre;
        db.buscarProductos(nombre)
        .then((data)=> res.json(data))
        .catch((err) => res.status(404).send(err.message));
      
})

router.delete('/borrar', (req,res)=>{
    const codigo = req.query.codigo;
    db.borrar(codigo)
    .then((data) => res.json(data))
    .catch((err) => res.status(404).send(err.message));
})

router.put('/actualizar', (req,res) =>{
    const producto = req.body;
    
    db.actualizar(producto.codigo, producto.nombre, producto.descripcion, producto.color, producto.material, producto.existencia, producto.precio)
    .then((data)=> res.json(data))
    .catch((err) =>{
        res.status(404).send(err.message);
    });
});
*/
module.exports = router;