create database DesarrolloWEB_PV;

use DesarrolloWEB_PV;

create table if not exists administrador (
id_ad int auto_increment not null primary key,
nombre varchar(45),
apellido varchar(45),
correo varchar(45),
password varchar(45)
);

create table if not exists sexo (
id_s int auto_increment not null primary key,
Nombre varchar(30)
);

create table if not exists usuario (
id_usr int auto_increment not null primary key,
nombre varchar(45),
apellido varchar(45),
correo varchar(45),
password varchar(45),
id_s int,

constraint  fksexo_usuaruio
foreign key (id_s)
references sexo(id_s)
);

create table if not exists direccion_tar(
id_direTar int auto_increment not null primary key,
calle varchar(45),
colonia varchar(45),
Ciudad varchar(45),
Estado varchar(45),
Pais varchar(45),
CP varchar(45)
);

create table if not exists tarjeta(
id_tar int auto_increment not null primary key,
nombre_titular varchar(45),
numero_tar varchar(45),
fecha_tar varchar(45),
cvv_tar varchar(45),
id_usr int,
id_direTar int,

constraint  fkusr_targeta
foreign key (id_usr)
references usuario(id_usr),

constraint  fkdirtar_targeta
foreign key (id_direTar)
references direccion_tar(id_direTar)
);



create table if not exists Estado_Venta(
id_EstV int auto_increment not null primary key,
Nombre varchar(30)
);

create table if not exists compra(
id_com int auto_increment not null primary key,
total double,
id_EstV int,
id_usr int,

constraint  fkusr_estV
foreign key (id_EstV)
references Estado_Venta(id_EstV),

constraint  fkusr_compra
foreign key (id_usr)
references usuario(id_usr)
);

create table if not exists materiales(
id_mat int auto_increment not null primary key,
Nombre varchar(30)
);

create table if not exists productos (
id_prod int auto_increment not null primary key,
nombre varchar(45),
descripcion varchar(45),
color  varchar(45),
Existencia int (40),
Precio double ,
id_mat int not null,

constraint  fkproduct_materiales
foreign key (id_mat)
references materiales(id_mat)
);


create table if not exists Carrito(
id_Car int auto_increment not null primary key,
cantidad int(20),
id_prod int,
id_com int,

constraint  fkcarrito_productos
foreign key (id_prod)
references productos(id_prod),

constraint  fkcarrito_compra
foreign key (id_com)
references compra(id_com)
);


